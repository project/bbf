CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Boolean Block Formatter module is used to render a pre-configured block
based on a toggle switch from a boolean field.

 * For a full description of the module visit:
   https://www.drupal.org/project/bbf

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/bbf


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Display Suite - https://www.drupal.org/project/ds


INSTALLATION
------------

 * Install the Boolean Block Formatter module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types > [Content type to
       edit] and add a boolean field.
    3. On the entity's display settings, set the display to use "Boolean Block
       Formatter" for the boolean field.
    4. Click on the contextual link icon and from the dropdown select the block
       you need to display on the field formatter settings. Update and Save.

Please note:
This module loads only the configured blocks, that means a block that is placed
in the block layout. It does not load plugins. You can place the block in the
Disabled section, if you only want to configure the block, but don't want to
show the block in a region.


MAINTAINERS
-----------

 * Doug Groene (dgroene) - https://www.drupal.org/u/dgroene
 * Vinoth Govindarajan (vinothg) - https://www.drupal.org/u/vinothg
