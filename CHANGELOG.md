# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed

### Added

### Changed

## [2.0.1] - 2023-9-08

### Fixed
- **#3005963:** Fixes issue of empty blocks being rendered.
- **#2982620:** Fixes issue of Readme.txt not following best practices. 

### Added
- **#3240230:** Added support for Drupal 9 and 10.

### Changed